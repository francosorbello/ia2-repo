## Caminos conectados

En este ejemplo los aviones solo pueden moverse entre aeropuertos que estén conectados. La conexión actual es:

     TF       SFN
     |         |
    MDZ--COR--AEP

### Estado inicial

COR tiene
- 1 avión disponible
- Autopartes

MDZ tiene
- Ningun avión disponible
- Cosechadoras

TF tiene
- Ningun avión disponible
- Tela para granizo

AEP tiene
- 3 aviones disponibles
- Fertilizante

SFN tiene
- 2 aviones disponibles
- Ningun suministro

### Objetivo

El objetivo es transportar la tela granizo, la cosechadora y las autopartes a AEP. Dado el estado inicial, MDZ no tiene aviones para llevar la cosechadora, por lo que se debe ir a buscar la carga. Además, TF almacena la tela para granizo pero no tiene conexión con AEP. Finalmente, las autopartes están en COR, que sirve de punto medio entre los otros 2 aeropuertos y el destino.

### Ejecucion

~/downward/fast-downward.py aviones.pddl aviones-problem.pddl --search "astar(lmcut())"