(define (domain capp)
(:requirements :equality :strips :action-costs)
(:predicates
	(orientacion ?o) ;(+-x,+-y,+-z)
	(feature ?f) ;fetures de la figura(ej: s2, h1 , eetc)
    (tipo ?t) ;tipo de feature(slot, hole, pass-through)
    (operacion ?op) ;torneado, fresado, taladrado
    (feature-tipo ?f ?feat-tipo) ;relaciona una feature con su tipo (ej: s2, slot)
    (orientacion-pieza ?oa)
    (orientacion-feature ?f ?oa) 
    (fabricable ?feat-tipo ?operacion) ;relaciona una feature con una operacion (ej: hole, taladrado)
    (fabricada ?feat) ;indica cuando una feature ha sido fabricada
    (herramienta ?tool ?op);indica si un objeto es una herramienta y que operacion realiza
    (montar-tool ?tool)
) 

(:functions
    (total-cost) ;funcion de costo para acciones
)

(:action setup-orientacion
 :parameters ( ?orientacion-inicial ?orientacion-final )
 :precondition
	(and 
        (orientacion-pieza ?orientacion-inicial) 
        (orientacion ?orientacion-inicial) 
        (orientacion ?orientacion-final)
    )
 :effect
	(and 
		(orientacion-pieza ?orientacion-final)
		(not (orientacion-pieza ?orientacion-inicial))
        (increase (total-cost) 1)
	)
)

;permite cambiar la herramienta montada en la maquina
(:action setup-maquina
    :parameters (?toolActual ?opActual ?toolSig ?opSig)
    :precondition 
    (and
        (herramienta ?toolActual ?opActual)
        (herramienta ?toolSig ?opSig)
        (montar-tool ?toolActual)
    )
    :effect (and 
        (not (montar-tool ?toolActual))
        (montar-tool ?toolSig)
        (increase (total-cost) 3) 
        ;cambiar la herramienta deberia ser mas caro que usar la herramienta actual para hacer la feature
        ;preguntar si mover la pieza deberia ser mas barato
    )
)

(:action op-fresado
 :parameters ( ?o ?f ?ft ?oper ?tool)
 :precondition
	(and 
        (orientacion-pieza ?o)
        (orientacion-feature ?f ?o)
        (orientacion ?o) 
        (feature ?f)
        (tipo ?ft)
        (feature-tipo ?f ?ft)
        (fabricable ?ft ?oper)
        (operacion ?oper)
        (= ?oper fresado)
        ;me aseguro que la herramienta puede realizar fresado y que esta montada
        (herramienta ?tool ?oper)
        (montar-tool ?tool)
    )
 :effect
    (and 
        (fabricada ?f) 
        (increase (total-cost) 1)
    )
)

(:action op-taladrado
    :parameters ( ?o ?f ?ft ?oper ?tool)
    :precondition
    (and 
        (orientacion-pieza ?o)
        (orientacion-feature ?f ?o)
        (orientacion ?o) 
        (feature ?f)
        (tipo ?ft)
        (feature-tipo ?f ?ft)
        (fabricable ?ft ?oper)
        (operacion ?oper)
        (= ?oper taladrado)
        ;me aseguro que la herramienta puede realizar taladrado y que esta montada
        (herramienta ?tool ?oper)
        (montar-tool ?tool)
    )
    :effect (and 
        (fabricada ?f)
        (increase (total-cost) 1)
    )
)

)
