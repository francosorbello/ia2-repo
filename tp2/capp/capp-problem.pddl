(define (problem capp-pieza)
    (:domain capp)
    (:objects 
        ;#--Direcciones--#
        xPos
        yPos
        zPos
        xNeg
        yNeg
        zNeg
        ;#--features
        s2
        s4
        s6
        s9
        s10
        h1
        h3
        h5
        h7
        h9
        h11
        h12
        ;#--tipos de features
        slot
        through-hole
        blind-hole
        ;#--operaciones
        fresado
        taladrado
        torneado
        ;#--herramientas
        fresa
        taladro
    )
    (:init 
        ;#--orientaciones
        (orientacion xPos)
        (orientacion yPos)
        (orientacion zPos)
        (orientacion xNeg)
        (orientacion yNeg)
        (orientacion zNeg)
        ;#--features
        (feature s2)
        (feature s4)
        (feature s6)
        (feature s9)
        (feature s10)
        (feature h1 )
        (feature h11)
        (feature h12)
        (feature h3 )
        (feature h5 )
        (feature h7 )
        (feature h9 )
        ;#--tipos de feature
        (tipo slot)
        (tipo through-hole)
        (tipo blind-hole)
        ;#--operaciones
        (operacion fresado)
        (operacion taladrado)
        (operacion torneado)
        ;#--feature y tipo
        (feature-tipo s2 slot)
        (feature-tipo s4 slot)
        (feature-tipo s6 slot)
        (feature-tipo s9 slot)
        (feature-tipo s10 slot)
        (feature-tipo h1 blind-hole)
        (feature-tipo h11 blind-hole) ;esto esta bien???
        (feature-tipo h12 blind-hole)
        (feature-tipo h3 through-hole)
        (feature-tipo h5 through-hole)
        (feature-tipo h7 through-hole)
        (feature-tipo h9 through-hole)
        ;#--orientacion pieza
        (orientacion-pieza xNeg)
        ;#--orientacion features
        (orientacion-feature s2 xPos)
        (orientacion-feature s4 xNeg)
        (orientacion-feature s6 xPos)
        (orientacion-feature s9 zPos)
        (orientacion-feature s10 zPos)
        (orientacion-feature h1 zPos)
        (orientacion-feature h11 xPos)
        (orientacion-feature h12 xPos)
        (orientacion-feature h3 zPos)
        (orientacion-feature h5 zPos)
        (orientacion-feature h7 xPos)
        (orientacion-feature h9 xPos)
        ;#--feature con operacion
        (fabricable slot fresado)
        (fabricable blind-hole taladrado)
        (fabricable through-hole taladrado)
        ;#--herramientas
        (herramienta fresa fresado)
        (herramienta taladro taladrado)
        (montar-tool fresa)
    )
    (:goal 
        (and
            (fabricada s2)
            (fabricada s4)
            (fabricada s6)
            (fabricada s9)
            (fabricada s10)
            (fabricada h1 )
            (fabricada h11)
            (fabricada h12)
            (fabricada h3 )
            (fabricada h5 )
            (fabricada h7 )
            (fabricada h9 )
        )
    )
)