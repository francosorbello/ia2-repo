import numpy as np
import matplotlib.pyplot as plt


def generar_datos_regresion(numero_ejemplos,numero_entradas,graficar = True):
    
    MOD_T = 1/numero_ejemplos
    FREC = 30
    AMPLITUD = 1 / 30

    x = np.zeros((numero_ejemplos,1))
    t = np.zeros((numero_ejemplos,numero_entradas))
    randomgen = np.random.default_rng()

    for i in range(numero_ejemplos):
        t[i] = i*MOD_T
        x[i] = t[i] + AMPLITUD * np.math.sin(FREC * t[i])
        # x[i] = t[i] + randomgen.standard_normal()
        # x[i] = t[i] + np.math.sin(t[i]) / AMPLITUD
    
    if graficar:
        plt.plot(t, x)
        plt.show()

    return t, x


def inicializar_pesos(n_entrada, n_capa_2, n_capa_3):
    randomgen = np.random.default_rng()

    w1 = 0.1 * randomgen.standard_normal((n_entrada, n_capa_2))
    b1 = 0.1 * randomgen.standard_normal((1, n_capa_2))

    w2 = 0.1 * randomgen.standard_normal((n_capa_2, n_capa_3))
    b2 = 0.1 * randomgen.standard_normal((1,n_capa_3))

    return {"w1": w1, "b1": b1, "w2": w2, "b2": b2}


def ejecutar_adelante(x, pesos):
    # Funcion de entrada (a.k.a. "regla de propagacion") para la primera capa oculta
    z = x.dot(pesos["w1"]) + pesos["b1"]

    # Funcion de activacion sigmoide
    h = 1 / (1 + np.exp(-z))

    # Salida de la red (funcion de activacion lineal). Esto incluye la salida de todas
    # las neuronas y para todos los ejemplos proporcionados
    y = h.dot(pesos["w2"]) + pesos["b2"]

    return {"z": z, "h": h, "y": y}


# x: n entradas para cada uno de los m ejemplos(nxm)
# t: salida correcta (target) para cada uno de los m ejemplos (m x 1)
# pesos: pesos (W y b)
def train(x, t, pesos, learning_rate, epochs,graficar_loss = True):
    # Cantidad de filas (i.e. cantidad de ejemplos)
    m = np.size(x, 0) 
    losses = np.zeros((epochs,1))
    
    for i in range(epochs):
        # Ejecucion de la red hacia adelante
        resultados_feed_forward = ejecutar_adelante(x, pesos)
        y = resultados_feed_forward["y"]
        h = resultados_feed_forward["h"]
        z = resultados_feed_forward["z"]

        # LOSS
        mse = np.square(t - y)
        loss = mse.mean()
        losses[i] = loss

        # Mostramos solo cada 1000 epochs
        if i %1000 == 0:
            print("Loss epoch", i, ":", loss)

        # Extraemos los pesos a variables locales
        w1 = pesos["w1"]
        b1 = pesos["b1"]
        w2 = pesos["w2"]
        b2 = pesos["b2"]

        # Ajustamos los pesos: Backpropagation
        dL_dy = 2*(y-t)
        dL_dy /= m

        #Calculo w2 y b2
        dL_dw2 = h.T.dot(dL_dy)                         # Ajuste para w2
        dL_db2 = np.sum(dL_dy, axis=0, keepdims=True)   # Ajuste para b2, sumando a lo largo de las filas

        #paso intermedio para w1 y b1
        dL_dh = dL_dy.dot(w2.T)                         
        
        sigma = h.T.dot(1 - h) #funcion sigmoide. transpongo para poder realizar el calculo
        dL_dz = dL_dh.dot(sigma)

        #calculo w1 y b1
        dL_dw1 = x.T.dot(dL_dz)                         # Ajuste para w1
        dL_db1 = np.sum(dL_dz, axis=0, keepdims=True)   # Ajuste para b1

        # Aplicamos el ajuste a los pesos
        w1 += -learning_rate * dL_dw1
        b1 += -learning_rate * dL_db1
        w2 += -learning_rate * dL_dw2
        b2 += -learning_rate * dL_db2

        # Actualizamos la estructura de pesos
        # Extraemos los pesos a variables locales
        pesos["w1"] = w1
        pesos["b1"] = b1
        pesos["w2"] = w2
        pesos["b2"] = b2

    if graficar_loss:
        eps = np.arange(start=0,stop=epochs,step=1)
        plt.plot(eps,losses)
        plt.show()

def iniciar(numero_clases, numero_ejemplos, graficar_datos):
    # Generamos datos
    x, t = generar_datos_regresion(numero_ejemplos,1)

    # Inicializa pesos de la red
    NEURONAS_CAPA_OCULTA = 100
    NEURONAS_ENTRADA = 1 #PREGUNTAR COMO HACER CON MAS DE 1 ENTRADA
    NEURONAS_SALIDA = 1
    pesos = inicializar_pesos(n_entrada=NEURONAS_ENTRADA, n_capa_2=NEURONAS_CAPA_OCULTA, n_capa_3=NEURONAS_SALIDA)

    # Entrena
    LEARNING_RATE=1
    EPOCHS=10000
    train(x, t, pesos, LEARNING_RATE, EPOCHS)


iniciar(numero_clases=3, numero_ejemplos=300, graficar_datos=False)