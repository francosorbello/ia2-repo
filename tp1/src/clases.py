#region Funciones de pertenencia
class FunctionType:
    """Funcion de pertenencia"""

    def __init__(self,mini,maxi,center):
        self.min = mini;
        self.max = maxi;
        self.center = center;

    def Evaluate(self,x):
        """retorna el valor de pertenencia de x"""
        return x
    def EvaluateY(self,y):
        return y

class Triangular(FunctionType):
    """funcion de la forma: /\ """
    def __init__(self,min,max,center):
        super().__init__(min,max,center)
    
    def Evaluate(self,x):
        rta = 0
        if self.min <= x <= self.center:
            rta = (x-self.min) / (self.center-self.min)
        if self.center < x <= self.max:
            rta = (self.max-x) / (self.max-self.center)
    
        return rta

    def EvaluateY(self, y):
        if(y < 0): return 0
        rta = 0
        if y <= 1:
            rta = self.max - y*(self.max - self.center)
        else:
            rta = y * (self.center-self.min) + self.min
        
        return rta

class Asimetric(FunctionType):
    """funcion de la forma ~-\ o /-~"""

    def __init__(self, maxi, center,isLeft):
        if(isLeft):
            super().__init__(0, maxi, center)
        else:
            super().__init__(maxi,0, center)
        self.isLeft = isLeft;
    
    def Evaluate(self, x):
        rta = 0
        if(self.isLeft == True):
            rta = self.EvaluateLeft(x)
        else:
            rta = self.EvaluateRight(x)
        return rta

    def EvaluateY(self, y):
        rta = 0
        if(self.isLeft == True):
            rta = self.EvaluateLeftY(y)
        else:
            rta = self.EvaluateRightY(y)
        return rta

    #~-\
    def EvaluateLeft(self,x):
        rta = 0
        if(x<=self.center):
            rta = 1
        elif self.center < x <= self.max:
            rta = (self.max-x) / (self.max-self.center)
        return rta

    def EvaluateLeftY(self,y):
        rta = 0
        if(y==1):
            rta = self.center
        elif 0 <= y < 1:
            rta = self.max - y*(self.max-self.center)
        return rta

    #/-~
    def EvaluateRight(self,x):
        rta = 0
        if(self.min <= x <=self.center):
            rta = (x-self.min) / (self.center-self.min)
        elif self.center < x:
            rta = 1
        return rta

    def EvaluateRightY(self,y):
        rta = 0
        if y == 1:
            rta = self.center
        elif 0 <= y < 1:
            rta = y*(self.center-self.min) + self.min
        return rta

#endregion


class FuzzySet:
    """Clase utilizada para representar un conjunto borroso

    Atributos
    -------
    mu : FunctionType
        Funcion de pertenencia al conjunto
    """
    def __init__(self,mu: FunctionType):
        self.mu = mu;

    def GetMembershipValue(self,x):
        """retorna el valor de pertenencia del parametro ingresado"""
        return self.mu.Evaluate(x)

class Variable:
    """Esta clase representa una variable linguistica
    
    Atributos
    ----------
    ng : FuzzySet
        Conjunto borroso para negativo grande

    np : FuzzySet
        Conjunto borroso para negativo pequeño

    z : FuzzySet
        Conjunto borroso para cero

    pp : FuzzySet
        Conjunto borroso para positivo pequeño

    ng : FuzzySet
        Conjunto borroso para positivo grande

    """
    def __init__(self,ng: FuzzySet,np: FuzzySet,z: FuzzySet,pp: FuzzySet,pg: FuzzySet):
        self.NG = ng
        self.NP = np
        self.Z = z
        self.PP = pp;
        self.PG = pg

    def Fuzzify(self,x):
        """devuelve los valores de pertenencia de un x a cada conjunto de la Variable"""
        rta = FuzzyVariable(
            self.NG.GetMembershipValue(x),
            self.NP.GetMembershipValue(x),
            self.Z.GetMembershipValue(x),
            self.PP.GetMembershipValue(x),
            self.PG.GetMembershipValue(x)
        )
        return rta
    
    def GetCenterSum(self):
        """Retorna la suma de los centros de cada conjunto difuso"""
        return self.NG.mu.center + self.NP.mu.center + self.Z.mu.center + self.PP.mu.center + + self.PG.mu.center


class FuzzyVariable:
    """Clase que guarda los resultados de fuzzificar un valor"""
    def __init__(self,ng=0,np=0,z=0,pp=0,pg=0):
        self.NG = ng
        self.NP = np
        self.Z = z
        self.PP = pp
        self.PG = pg
    
    def GetMaxValue(self):
        return max(self.NG ,self.NP, self.Z, self.PP, self.PG)
    
    def AddValues(self):
        """suma de todos los valores fuzzificados"""
        return self.NG + self.NP + self.Z + self.PP + self.PG

    def __str__(self) -> str:
        s1 = "NG: "+str(self.NG)+"\n"
        s2 = "NP: "+str(self.NP)+"\n"
        s3 = "Z : "+str(self.Z)+"\n"
        s4 = "PP: "+str(self.PP)+"\n"
        s5 = "PG: "+str(self.PG)+"\n"
        return (s1+s2+s3+s4+s5)
        
