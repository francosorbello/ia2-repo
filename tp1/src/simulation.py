from math import pi
import numpy as np
from scipy import constants
import clases

CONSTANTE_M = 2 # Masa del carro
CONSTANTE_m = 1 # Masa de la pertiga
CONSTANTE_l = 1 # Longitud dela pertiga

# Simula el modelo del carro-pendulo.
# Parametros:
#   t_max: tiempo maximo (inicia en 0)
#   delta_t: incremento de tiempo en cada iteracion
#   theta_0: Angulo inicial (grados)
#   v_0: Velocidad angular inicial (radianes/s)
#   a_0: Aceleracion angular inicial (radianes/s2)
def simular(t_max, delta_t, theta_0, v_0, a_0):
    theta = (theta_0 * np.pi) / 180
    v = v_0
    a = a_0
    f = 0
    extremos_theta = [-pi, -pi/4, 0, pi/4, pi]
    # extremos_theta = [-30, -10, 0, 10, 30]
    tNG = clases.Asimetric(extremos_theta[1],extremos_theta[0],True)
    tNP = clases.Triangular(extremos_theta[0],extremos_theta[2],extremos_theta[1])
    tZ = clases.Triangular(extremos_theta[1],extremos_theta[3],extremos_theta[2])
    tPP = clases.Triangular(extremos_theta[2],extremos_theta[4],extremos_theta[3])
    tPG = clases.Asimetric(extremos_theta[3],extremos_theta[4],False)
    Theta = clases.Variable(
        clases.FuzzySet(tNG),
        clases.FuzzySet(tNP),
        clases.FuzzySet(tZ),
        clases.FuzzySet(tPP),
        clases.FuzzySet(tPG)
    )

    #--armo variable velocidad angular--
    #conjuntos borrosos
    extremos_vel = [-10, -5, 0, 5, 10]
    vNG = clases.Asimetric(extremos_vel[1],extremos_vel[0],True)
    vNP = clases.Triangular(extremos_vel[0],0,extremos_vel[1])
    vZ = clases.Triangular(extremos_vel[1],extremos_vel[3],0)
    vPP = clases.Triangular(0,extremos_vel[4],extremos_vel[3])
    vPG = clases.Asimetric(extremos_vel[3],extremos_vel[4],False)
    Vel = clases.Variable(
        clases.FuzzySet(vNG),
        clases.FuzzySet(vNP),
        clases.FuzzySet(vZ),
        clases.FuzzySet(vPP),
        clases.FuzzySet(vPG)
    )

    #--armo variable fuerza--
    #conjuntos borrosos
    extremos_fuerza = [-10,-5,0,5,10]

    fNG = clases.Asimetric(extremos_fuerza[1],extremos_fuerza[0],True)
    fNP = clases.Triangular(extremos_fuerza[0],extremos_fuerza[2],extremos_fuerza[1])
    fZ = clases.Triangular(extremos_fuerza[1],extremos_fuerza[3],extremos_fuerza[2])
    fPP = clases.Triangular(extremos_fuerza[2],extremos_fuerza[4],extremos_fuerza[3])
    fPG = clases.Asimetric(extremos_fuerza[3],extremos_fuerza[4],False)
    Fuerza = clases.Variable(
        clases.FuzzySet(fNG),
        clases.FuzzySet(fNP),
        clases.FuzzySet(fZ),
        clases.FuzzySet(fPP),
        clases.FuzzySet(fPG)
    )

    # Simular
    x = np.arange(0, t_max, delta_t)
    for t in x:
        a = calcula_aceleracion(theta, v, f)
        v = v + a * delta_t
        theta = theta + v * delta_t + a * np.power(delta_t, 2) / 2
        if(theta > pi):
            theta = theta - 2*pi

        if(theta < -pi):
            theta = 2*pi + theta
        currentForce = calcular_fuerza(Theta.Fuzzify(theta),Vel.Fuzzify(v))
        f = -calcular_media_centros(currentForce,Fuerza)
        # if( round(t*10) % 5 == 0 and t >= 1 ):
        print(str(round(theta,2))+" | "+str(round(v,2))+" | "+str(round(f,2))+" | "+str(round(a,2)))

        #print(theta|velocidad|fuerza|aceleracion)

# Calcula la aceleracion en el siguiente instante de tiempo dado el angulo y la velocidad angular actual, y la fuerza ejercida
def calcula_aceleracion(theta, v, f):
    numerador = constants.g * np.sin(theta) + np.cos(theta) * ((-f - CONSTANTE_m * CONSTANTE_l * np.power(v, 2) * np.sin(theta)) / (CONSTANTE_M + CONSTANTE_m))
    denominador = CONSTANTE_l * (4/3 - (CONSTANTE_m * np.power(np.cos(theta), 2) / (CONSTANTE_M + CONSTANTE_m)))
    return numerador / denominador

def calcular_fuerza(theta: clases.FuzzyVariable,vel: clases.FuzzyVariable) -> clases.FuzzyVariable:
  #dados un theta y una velocidad, fuzzyfico y paso los conjuntos borrosos obtenidos por las reglas de inferencia
  currentTheta = theta
  currentVel = vel

  #reglas con el mismo consecuente
  fPG = max(
      min(currentTheta.NG,currentVel.NG),
      min(currentTheta.NP,currentVel.NG),
      min(currentTheta.Z ,currentVel.NG),
      min(currentTheta.NG,currentVel.NP)
  )
  fPP = max(
      min(currentTheta.NG,currentVel.Z),
      min(currentTheta.NG,currentVel.PP),
      min(currentTheta.NP,currentVel.NP),
      min(currentTheta.Z ,currentVel.NP)
  )
  fZ = max(
      min(currentTheta.NG,currentVel.PG),
      min(currentTheta.NP,currentVel.Z),
      min(currentTheta.NP,currentVel.PP),
      min(currentTheta.Z ,currentVel.Z),
      min(currentTheta.PP,currentVel.NP)
  )
  fNP = max(
      min(currentTheta.NP,currentVel.PG),
      min(currentTheta.Z ,currentVel.PP),
      min(currentTheta.PP,currentVel.NG),
      min(currentTheta.PP,currentVel.Z),
      min(currentTheta.PG,currentVel.NG),
      min(currentTheta.PG,currentVel.PP)
  )
  fNG = max(
      min(currentTheta.Z ,currentVel.PG),
      min(currentTheta.PP,currentVel.PP),
      min(currentTheta.PP,currentVel.PG),
      min(currentTheta.PG,currentVel.NP),
      min(currentTheta.PG,currentVel.Z),
      min(currentTheta.PG,currentVel.PG)
  )

  return clases.FuzzyVariable(fNG,fNP,fZ,fPP,fPG)

def calcular_media_centros(fuerzaActual: clases.FuzzyVariable, fuerza: clases.Variable):
  fng = fuerzaActual.NG * fuerza.NG.mu.center
  fnp = fuerzaActual.NP * fuerza.NP.mu.center
  fz = fuerzaActual.Z * fuerza.Z.mu.center
  fpp = fuerzaActual.PP * fuerza.PP.mu.center
  fpg = fuerzaActual.PG * fuerza.PG.mu.center
  
  media = fng + fnp + fz + fpp + fpg
  media = media / fuerzaActual.AddValues()
  return media

simular(10, 0.0001, 45, 0, 0)